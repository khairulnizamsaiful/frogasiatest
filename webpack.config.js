var path = require('path');
var webpack = require('webpack');
 
module.exports = {
  devtool: 'inline-source-map',
  entry: [
		'webpack-dev-server/client?http://127.0.0.1:9091/',
		'webpack/hot/only-dev-server',
		'babel-polyfill',
		'./src'
	],
  output: { 
    path: path.join(__dirname,'dist'), 
    filename: 'bundle.js' 
  },
  resolve: {
      modulesDirectories: ['node_modules', 'src'],
      extensions: ['', '.js']
  },
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react'],
        }
      }
    ]
  },
	plugins: [
			new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
	]
};