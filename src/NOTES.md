<!---
Add your comments / notes and thoughts to this doc

Any special instructions to running your code?
-->
##Frog game of life
Grow your frog pond community by mating. Don't stop mating because your community will die. Continue to mate and have a happy and healthy frog family

###Build
```
npm install
```

###Run
```
npm run test
```
Then open http://localhost:9091 on your browser 

###Final Comments
I am mainly a front-end developer currently building apps with React.js framework. I created this simple app/game to show the possibility of interactivity using just front-end. It took me a total of 3 hours from start of setup to end of coding.