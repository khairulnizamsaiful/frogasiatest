import React from 'react'
import FrogListItem from 'js/frog-li'

const MALE = "M";
const FEMALE = "F";

const INTERVAL = 5000;

const N_CHILDREN_MAX = 5;
const N_AGE_MATE_MIN = 3;
const N_AGE_MATE_MAX = 12;
const N_AGE_LIFE_MIN = 8;
const N_AGE_LIFE_MAX = 20;

export default class App extends React.Component {

	constructor(props){
		super(props);

		//initial state with 2 frogs
		this.state = {
			frogs : [
				{
					id: 0,
					name: "Kermit",
					sex: MALE,
					dob: 0,
					hp: 100
				},
				{
					id: 1,
					name: "Marla",
					sex: FEMALE,
					dob: 0,
					hp: 100
				}
			],
			time: 0,
			log: []
		}
	}

	componentWillMount() {
		//start timer once component is mounted
		var timer = setInterval(this.increaseTime.bind(this), INTERVAL);
		this.setState({timer});
	}

	increaseTime() {
		// increase time by 1 and make old frogs die
		const {time, frogs, log, timer} = this.state;
		var msg = [];

		//select which pool of frogs to test to die
		var oldFrogs = frogs.filter(f=>time-f.dob>N_AGE_LIFE_MIN);
		oldFrogs.forEach(f=>{
			var index = frogs.indexOf(f);

			//random die or if frog is too old
			var shouldDie = Math.random() < 0.5 || time-f.dob>N_AGE_LIFE_MAX;
			if(shouldDie) {
				msg.push((f.name || "Unnamed") + " is dead at the age of " + (time-f.dob) + "! :(");
				frogs.splice(index, 1);
			}
		});

		//log if community has no more frog
		if(frogs.length === 0) {
			msg.push("Your community died on time: " + (time+1));
			clearInterval(timer);
		}

		//update state
		this.setState({time: time+1, frogs, log: [...log, ...msg]});
	}

	createFrog(id, name, sex, parentIds, dob) {
		//create frog object
		return {
			id,
			sex,
			dob,
			parentIds,
			hp: 100
		}
	}

	nameChanged(frog, newName) {
		//handle frog name change
		const {frogs} = this.state;
		frog.name = newName;
		this.setState({frogs});
	}

	mateFrog(frog1, frog2) {
		//handle frog mating
		const {frogs, time, log} = this.state;
		var n = 0, msg = [], canMate = true;

		if(time - frog1.dob < N_AGE_MATE_MIN || time - frog2.dob < N_AGE_MATE_MIN) {
			//frog is too young
			msg.push(frog1.name + " or " + frog2.name + " is too young to mate!")
			canMate = false;

		} else if(time - frog1.dob > N_AGE_MATE_MAX || time - frog2.dob > N_AGE_MATE_MAX) {
			//frog is too old
			msg.push(frog1.name + " or " + frog2.name + " is too old to mate!")
			canMate = false;
		
	} else if(frog1.sex === frog2.sex) {
			//frog is of same sex
			msg.push("Same sex mating is not allowed...");
			canMate = false;	
	} else {
			var fFrog = frog1.sex === FEMALE ? frog1 : frog2;
			var cFrogs = frogs.filter(f=>f.parentIds && f.parentIds.indexOf(fFrog.id) > -1);
			if(cFrogs.length) {
				//female frog has just delivered
				var maxDOB = cFrogs.map(f=>f.dob).reduce((a,b)=>Math.max(a,b));
				if(maxDOB === time) {
					msg.push(fFrog.name + " has just conceived. Please wait to mate her again.");
					canMate = false;
				}
			}
		}

		var children = [];
		if(canMate) {
			//generate n number of children
			if(frog1 && frog2 && frog1.sex !== frog2.sex) {
				n = Math.round(Math.random()*N_CHILDREN_MAX);
			}
			var maxId = frogs.map(f=>f.id).reduce((a,b)=>Math.max(a,b));
			for(var i=0; i<n; i++) {
				var sex = [MALE, FEMALE][Math.round(Math.random())];
				children.push(this.createFrog(++maxId, null, sex, [frog1.id, frog2.id], time));
			}
			if(n) msg.push(frog1.name + " and " + frog2.name + " conceived " + n + " children!")
		}

		//update state
		this.setState({ frogs: [...frogs, ...children], log: [...log, msg]});
	}

	render() {
		const {frogs, time, log} = this.state;
		return (
			<div style={{maxWidth: '500px', margin: 'auto'}}>
				<h1>Hello frogs!</h1>
				<p>Grow your frog pond community by mating. Don't stop mating because your community will die. Continue to mate and have a happy and healthy frog family!</p>
				<h2>Current time: {time}</h2>
				<table>
					<thead>
						<tr>
							<th>Frog name</th>
							<th>Age</th>
							<th>Gender</th>
							<th>Mate</th>
						</tr>
					</thead>
					<tbody>
						{frogs.map(f=>
							<FrogListItem 
								key={f.id} 
								frog={f} 
								{...this.state} 
								onNameChanged={this.nameChanged.bind(this)}
								onMate={this.mateFrog.bind(this)}/>
						)}
					</tbody>
				</table>
				<h2>Log:</h2>
				<ul>
					{[...log].reverse().map((l,li)=><li key={li}>{l}</li>)}
				</ul>
			</div>

		)
	}
}