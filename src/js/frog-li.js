import React from 'react'

export default class FrogListItem extends React.Component {
	constructor(props){
		super(props);
		this.state = {};
	}

	handleMateChanged(e) {
		this.setState({mateId: e.target.value})
	}

	handleNameChanged(e) {
		const {onNameChanged, frog} = this.props;
		onNameChanged(frog, e.target.value);
	}

	render() {
		const {frog, frogs, time, onMate} = this.props;
		const {mateId} = this.state;
		var mate = mateId ? frogs[frogs.map(f=>f.id).indexOf(parseInt(mateId))] : null;
		return (
			<tr>
				<td><input type="text" value={frog.name} onChange={this.handleNameChanged.bind(this)}/></td>
				<td>{time - frog.dob}</td>
				<td>{frog.sex}</td>
				<td>
					Mate: 
					<select value={mateId || "null"} onChange={this.handleMateChanged.bind(this)}>
						<option disabled value="null">Select a frog</option>
						{ frogs.filter(f=>f!==frog && f.name).map(f=><option key={f.id} value={f.id}>{f.name}</option>)}
					</select>
					<button disabled={!mateId || mateId<0} onClick={()=>onMate(frog, mate)}>Mate!</button>
				</td>
			</tr>
		)
	}
}